#ifndef FORWARD_H
#define FORWARD_H
#include "list.h"

#include <iostream>

#include <cstddef>  // For std::ptrdiff_t
#include <iterator> // For std::forward_iterator_tag

template <typename T> struct Forward_Node {
  T data;
  Forward_Node<T> *next = nullptr;
  // https://internalpointers.com/post/writing-custom-iterators-modern-cpp
public:
  Forward_Node(T value, Forward_Node<T> *puntero = nullptr)
      : data(value), next(puntero){};

  void killSelf() {
    // TODO
  }

  using iterator_category = std::forward_iterator_tag;
  using difference_type = std::ptrdiff_t;
  using value_type = T;
  using pointer = Forward_Node<T> *;   // or also value_type*
  using reference = Forward_Node<T> &; // or also value_type&

  reference operator*() { return *this; }
  pointer operator->() { return this; }

  // Prefix increment
  Forward_Node &operator++() {
    this->next;
    return *this;
  }

  // Postfix increment
  Forward_Node operator++(int) {
    Forward_Node tmp = *this;
    ++(*this);
    return tmp;
  }

  friend bool operator==(const Forward_Node &a, const Forward_Node &b) {
    return a.data == b.data;
  };
  friend bool operator!=(const Forward_Node &a, const Forward_Node &b) {
    return a.data != b.data;
  };
};

// TODO: Implement all methods
template <typename T> class ForwardList : public List<T> {
  Forward_Node<T> *head;
  // T at(int index) { return this->head->data; }

public:
  Forward_Node<T> begin() { return *head; } // revisar sintaxis
  Forward_Node<T> end() {
    Forward_Node<T> *respuesta = head; // revisar sintaxis

    while (respuesta != nullptr) {
      respuesta = respuesta->next;
    }

    return *respuesta;
  }

  // ForwardList() : List<T>() {}
  ~ForwardList() {
    // TODO
  }
  // revisar a partir de aqui
  T front() { return head->data; } // error cuando no hay elementos

  T back() { // error cuando no hay elementos
    Forward_Node<T> *puntero = head;

    while (puntero != nullptr) {
      puntero = puntero->next; // posible error
    }

    return puntero->data;
  }

  void push_front(T elemento) {
    Forward_Node<T> *antiguo = head;

    head = new Forward_Node<T>(elemento, antiguo);
  }

  void push_back(T elemento) {
    Forward_Node<T> *puntero = head;

    while (puntero != nullptr) {
      puntero = puntero->next;
    }
    puntero = new Forward_Node(elemento);
  }

  T pop_front() {
    auto nueva_cabeza = this->head->next;
    auto datos = this->head->data;
    delete head; // revisar

    this->head = nueva_cabeza;
    return datos;
  }

  T pop_back() { return T(); }

  T &operator[](int index) { return this->head->data; }

  int size() {
    int cont;
    auto puntero = this->head;

    while (puntero != nullptr) {
      puntero = puntero->next;
      cont++;
    }
    return cont;
  }

  bool is_empty() { return this->size() == 0; }

  void clear() {
    // no se que hace
  }

  T insert(T elemento, int index) {}

  bool remove(int index) {}

  void reverse() {}

  bool is_sorted() { return false; }

  void sort() {}

  std::string name() { return std::string("dfsd"); }
};

#endif